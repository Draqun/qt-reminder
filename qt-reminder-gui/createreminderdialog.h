/**
  * @author: Damian Giebas (Draqun)
  * @date: 15.05.2017
  * @version: 1.0
  */

#ifndef CREATEREMINDERDIALOG_H
#define CREATEREMINDERDIALOG_H

#include <QAbstractButton>
#include <QDialog>

namespace Ui {
class CreateReminderDialog;
}

class CreateReminderDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateReminderDialog(QWidget *parent = 0);
    ~CreateReminderDialog();

private slots:
    void onOkClicked();
    void onCancelClicked();

private:
    Ui::CreateReminderDialog *ui;
};

#endif // CREATEREMINDERDIALOG_H
