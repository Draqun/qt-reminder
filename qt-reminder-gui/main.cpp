/**
  * @author: Damian Giebas (Draqun)
  * @date: 15.05.2017
  * @version: 1.0
  */

#include <QApplication>

#include <mainwindow.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QApplication::setQuitOnLastWindowClosed(false);
    MainWindow w;
    w.hide();

    return a.exec();
}
