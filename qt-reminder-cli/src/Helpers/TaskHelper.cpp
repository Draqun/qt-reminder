/**
  * @author: Damian Giebas (Draqun)
  * @date: 16.08.2017
  * @version: 1.0
  */

#include <QtCore>

#include <Helpers/TaskHelper.h>

using namespace std;

namespace Helpers
{

TaskHelper::TaskHelper(QtReminder::Reminder &r, function<void (QString, QString)> showReminderAction, QObject *parent):
    QObject{parent}, r{&r}, showReminderAction{showReminderAction}, timer{new QTimer{this}}
{
    connect(this->timer, SIGNAL(timeout()), this, SLOT(showReminder()));
    this->r->addObserver(*this);
}

TaskHelper::~TaskHelper()
{
    this->timer->stop();
    disconnect(this->timer, SIGNAL(timeout()), 0, 0);

    delete this->timer;
    this->r->removeObserver(*this);
    qDebug()<<"[" <<__FUNCTION__ <<"]" <<"Removed for reminder: " <<QString::fromStdWString(*this->r->getTitle());
}

void TaskHelper::setAsSingleShotTask(QDateTime startdate)
{
    this->timer->setInterval(startdate.msecsTo(r->getRunTime()));
    this->timer->setSingleShot(true);
}

void TaskHelper::showReminder()
{
    this->showReminderAction(QString::fromStdWString(*r->getTitle()), QString::fromStdWString(*r->getDescription()));
    qDebug()<<r->getReminderId().toString() <<" " <<QString::fromStdWString(*r->getTitle());
    if (this->r->isSingleShot())
    {
        emit reminderShown(r->getReminderId());
    }
}

void TaskHelper::runTimer(QDateTime startdate)
{
    this->timer->start(startdate.msecsTo(r->getRunTime()));
}

void TaskHelper::notify()
{
    qDebug()<<"[" <<__FUNCTION__ <<"]" <<"I'm notified";
    delete this;
}

} // namespace Helpers
