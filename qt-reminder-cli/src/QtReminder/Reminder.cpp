/**
  * @author: Damian Giebas (Draqun)
  * @date: 26.05.2017
  * @version: 1.0
  */

#include <algorithm>

#include <QDebug>

#include <QtReminder/Reminder.h>

using namespace std;

namespace QtReminder
{

Reminder::Reminder(QDateTime &run_time, const shared_ptr<wstring> title, const shared_ptr<wstring> description,
                   bool cyclic):
    title{title}, description{description}, cyclic{cyclic}
{
    this->run_time.swap(run_time);
}

Reminder::Reminder(QDateTime &run_time, const shared_ptr<wstring> title, const shared_ptr<wstring> description,
                   QDateTime &created, bool cyclic):
    title{title}, description{description}, cyclic{cyclic}
{
    this->run_time.swap(run_time);
    this->created.swap(created);
}

Reminder::Reminder(const Reminder &r):
    title{r.title}, description{r.description}, reminder_id{r.reminder_id}, cyclic{r.cyclic}
{
    QDateTime tmp_created{r.created};
    QDateTime tmp_run_time{r.run_time};

    this->created.swap(tmp_created);
    this->run_time.swap(tmp_run_time);
}

std::wstring* Reminder::getTitle()
{
    return this->title.get();
}

std::wstring* Reminder::getDescription()
{
    return this->description.get();
}

QDateTime Reminder::getRunTime()
{
    return this->run_time;
}

bool Reminder::isCyclic()
{
    return this->cyclic;
}

bool Reminder::isSingleShot()
{
    return !this->cyclic;
}

Reminder& Reminder::operator =(const Reminder &r)
{
    QDateTime tmp_run_time{r.run_time};
    QDateTime tmp_created{r.created};

    this->created.swap(tmp_created);
    this->cyclic = r.cyclic;
    this->description = r.description;
    this->reminder_id = r.reminder_id;
    this->run_time.swap(tmp_run_time);
    this->title = r.title;
    return *this;
}

QUuid Reminder::getReminderId()
{
    return this->reminder_id;
}

Reminder::~Reminder()
{
    this->notifyObservers();
    qDebug()<<"[" <<__FUNCTION__ <<"]" <<"Destroying " <<QString::fromStdWString(*this->title.get()) <<" "
           <<QString::fromStdWString(*this->description.get());
}

void Reminder::addObserver(Helpers::Observer &o)
{
    qDebug()<<"[" <<__FUNCTION__ <<"]" <<"I add observer";
    qDebug()<<"[" <<__FUNCTION__ <<"]" <<"Number of observers before add one : " <<this->observers.size();
    this->observers.push_back(&o);
    qDebug()<<"[" <<__FUNCTION__ <<"]" <<"Number of observers after add one : " <<this->observers.size();
}

void Reminder::notifyObservers()
{
    qDebug()<<"[" <<__FUNCTION__ <<"]" <<"Number of observers to notify: " <<this->observers.size();
    for(auto o: this->observers)
    {
        qDebug()<<"[" <<__FUNCTION__ <<"]" <<"I notify observer";
        o->notify();
    }
}

void Reminder::removeObserver(Helpers::Observer &o)
{
    qDebug()<<"[" <<__FUNCTION__ <<"]" <<"I remove observer";
    qDebug()<<"[" <<__FUNCTION__ <<"]" <<"Number of observers before remove one : " <<this->observers.size();
    this->observers.erase(remove(observers.begin(), observers.end(), &o), observers.end());
    qDebug()<<"[" <<__FUNCTION__ <<"]" <<"Number of observers after remove one : " <<this->observers.size();
}

} // namespace QtReminder
