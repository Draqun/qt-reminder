/**
  * @author: Damian Giebas (Draqun)
  * @date: 26.05.2017
  * @version: 1.0
  */

#include <QDebug>

#include <QtReminder/ReminderManager.h>

namespace QtReminder
{

ReminderManager::ReminderManager(){}

ReminderManager::~ReminderManager()
{
    this->reminders.clear();
}

std::vector<Reminder*>::iterator ReminderManager::getElementIterator(const QUuid reminder_id)
{
    return std::find_if(std::begin(this->reminders), std::end(this->reminders),
                           [reminder_id](Reminder *r) { return r->getReminderId() == reminder_id; });
}

void ReminderManager::appendReminder(Reminder &r)
{
    this->reminders.push_back(&r);
}

void ReminderManager::removeReminder(const int index)
{
    qDebug()<<"[" <<__FUNCTION__ <<"]" <<"Size of reminders is " <<this->reminders.size();
    Reminder * el = this->reminders[index];
    this->reminders.erase(this->reminders.begin() + index);
    delete el;
    qDebug()<<"[" <<__FUNCTION__ <<"]" <<"Size of reminders is " <<this->reminders.size();
}

const int ReminderManager::getReminderIndex(const QUuid reminder_id)
{
    auto begin = this->reminders.begin();
    auto it = getElementIterator(reminder_id);
    return std::distance(begin, it);
}

} // namespace QtReminder
