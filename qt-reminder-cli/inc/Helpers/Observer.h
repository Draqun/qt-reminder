/**
  * @author: Damian Giebas (Draqun)
  * @date: 15.08.2017
  * @version: 1.0
  */

#ifndef OBSERVER
#define OBSERVER

namespace Helpers
{

/**
 * @brief The Observer class - Class for observers in Observer pattern
 */
class Observer
{
public:
    /**
     * @brief notify - pure virtual method for impletation in class TaskHelper
     */
    virtual void notify() = 0;
    /**
     * @brief ~Observer - Dtor
     */
    virtual ~Observer(){}
};

} // namespace Helpers
#endif // OBSERVER

