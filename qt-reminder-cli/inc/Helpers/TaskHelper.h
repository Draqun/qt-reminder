/**
  * @author: Damian Giebas (Draqun)
  * @date: 16.08.2017
  * @version: 1.0
  */

#ifndef TASKHELPER
#define TASKHELPER

#include <functional>

#include <QObject>
#include <QTimer>

#include <Helpers/Observer.h>
#include <QtReminder/Reminder.h>

namespace Helpers
{

class TaskHelper: public QObject, public Helpers::Observer
{
    Q_OBJECT

private:
    /**
     * @brief r - pointer to reminder
     */
    QtReminder::Reminder *r;

    /**
     * @brief timer - timer object used to run task
     */
    QTimer *timer;

    /**
     * @brief showReminderAction - function object used to remind user about task
     */
    std::function<void (QString, QString)> showReminderAction;

public:
    /**
     * Delete unused Ctors and assignment operators
     */
    TaskHelper() = delete;
    TaskHelper(const TaskHelper&) = delete;
    TaskHelper(TaskHelper&&) = delete;
    TaskHelper& operator=(const TaskHelper&) = delete;
    TaskHelper& operator=(TaskHelper&&) = delete;

    /**
     * @brief TaskHelper - Ctor
     * @param r - QtReminder::Reminder reference
     * @param showReminderAction - std::function object which run in showReminder with title and description of reminder
     * @param parent - Object pointer, nullptr default
     */
    TaskHelper(QtReminder::Reminder&, std::function<void (QString, QString)>, QObject* = nullptr);

    /**
     * @brief ~TaskHelper - virtual Dtor
     */
    virtual ~TaskHelper();

    /**
     * @brief runTimer - function starting timer work
     * @param startdate - start datetime object, defautl value is QDateTime::currentDateTime()
     */
    void runTimer(QDateTime = QDateTime::currentDateTime());

    /**
     * @brief notify - Override inherit virtual function
     */
    void notify() override;

    /**
     * @brief setAsSingleShotTask - function set task as a single shot.
     * @param startdate - start datetime object, defautl value is QDateTime::currentDateTime()
     */
    void setAsSingleShotTask(QDateTime = QDateTime::currentDateTime());

public slots:
    /**
     * @brief showReminder - Show reminder
     */
    void showReminder();

signals:
    /**
     * @brief reminderShown - signal send after show reminder. Parameter is a QUuid of shown reminder.
     */
    void reminderShown(const QUuid);

};

} // namespace Helpers
#endif // TASKHELPER

