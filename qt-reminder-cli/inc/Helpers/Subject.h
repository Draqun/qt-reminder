/**
  * @author: Damian Giebas (Draqun)
  * @date: 15.08.2017
  * @version: 1.0
  */

#ifndef SUBJECT
#define SUBJECT

#include <vector>

#include <Helpers/Observer.h>

namespace Helpers
{

/**
 * @brief The Subject class - Class for subjects in Observer pattern
 */
class Subject
{
protected:
    /**
     * @brief observers - list of pointers to observers
     */
    std::vector<Observer*> observers;

public:
    /**
     * @brief add_observer - Pure virtual method for adding observer
     * @param o - Observer object instance
     */
    virtual void addObserver(Observer &o) = 0;

    /**
     * @brief notify_observers - Pure virtual method notifying observer about changes
     */
    virtual void notifyObservers() = 0;

    /**
     * @brief remove_observer - Pure virtual method for removing observer
     * @param o - Observer object for remove
     */
    virtual void removeObserver(Observer &o) = 0;
};

} // namespace Helpers
#endif // SUBJECT
