/**
  * @author: Damian Giebas (Draqun)
  * @date: 26.05.2017
  * @version: 1.0
  */

#ifndef REMINDER
#define REMINDER
#include <memory>
#include <string>

#include <QDateTime>
#include <QUuid>

#include <Helpers/Subject.h>

namespace QtReminder
{
/**
 * @brief The Reminder class
 * Class inherit from Helpers::Subject to inform CyclicTaskHelper when it is destroyed.
 */
class Reminder: public Helpers::Subject
{

private:
    /// Time where object was created
    QDateTime created;

    /// Time when notification have to be run
    QDateTime run_time;

    /// Title of notification
    std::shared_ptr<std::wstring> title;

    /// Full description
    std::shared_ptr<std::wstring> description;

    /// Uuid
    QUuid reminder_id = QUuid::createUuid();

    /// Cyclic
    bool cyclic;

public:
    /**
     * @brief Reminder - Ctor
     * @param run_time - When notification have to be run_time
     * @param title - Title of notification
     * @param description - Its description
     * @param cyclic - Should be run cyclic
     */
    Reminder(QDateTime&, const std::shared_ptr<std::wstring>, const std::shared_ptr<std::wstring>, bool);

    /**
     * @brief Reminder - Ctor
     * @param run_time - When notification have to be run_time
     * @param title - Title of notification
     * @param description - Its description
     * @param created - When this reminder was created
     * @param cyclic - Should be run cyclic
     */
    Reminder(QDateTime&, const std::shared_ptr<std::wstring>, const std::shared_ptr<std::wstring>, QDateTime&, bool);

    /**
     * @brief Reminder - Ctor
     * @param r - reminder to copy
     */
    Reminder(const Reminder &);

    /**
     * @brief getTitle - Title of reminder
     * @return pointer to std::wstring
     */
    std::wstring* getTitle();

    /**
     * @brief getDescription - Description of reminder
     * @return pointer to std::wstring
     */
    std::wstring* getDescription();

    /**
     * @brief isCyclic - Function to check this is cyclic reminder
     * @return True if run cyclic, in other case false
     */
    bool isCyclic();

    /**
     * @brief isSingleShot - Function to check this is single shot reminder
     * @return True if is not cyclic, in other case false
     */
    bool isSingleShot();

    /**
     * @brief getRunTime - Getter for run_time
     * @return QDateTime object
     */
    QDateTime getRunTime();

    /**
     * @brief operator = Overrider operator
     * @return reference
     */
    Reminder &operator =(const Reminder &);

    /**
     * @brief getReminderId - getter for reminder_id
     * @return const QUuid
     */
    QUuid getReminderId();

    /**
      * @brief ~Reminder - Dtor
      */
    virtual ~Reminder();

    /**
     * @brief add_observer - Virtual method for adding observer
     * @param o - Observer object instance
     */
    void addObserver(Helpers::Observer&) override;

    /**
     * @brief notify_observers - Function notifying observer about changes
     */
    void notifyObservers() override;

    /**
     * @brief remove_observer - Function removing observer
     * @param o - Observer object for remove
     */
    void removeObserver(Helpers::Observer&) override;
};

} // namespace QtReminder
#endif // REMINDER

